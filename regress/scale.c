/*
 * Copyright (c) 2016 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dht.h>
#include <hashtab.h>
#include <pthread.h>
#include <err.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <sys/wait.h>


#define PORT_BASE	5000
#define	N_NODES		50
#define	N_REPLICAS	N_NODES 

#define	MAX_KEYS	1
struct v_node {
	char *id;
	char *ip;
	int port;
	int n_keys;
	char *keys[MAX_KEYS];
	char *vals[MAX_KEYS];
} *vnodes;

struct args {
	int nidx;
	struct dht_node *node;
};

#define TOTAL_KV	(N_NODES * MAX_KEYS)
char *all_keys[TOTAL_KV];
char *all_vals[TOTAL_KV];

void *
test_thread(void *arg)
{
	int i, fail = 0;
	int me = ((struct args *)arg)->nidx;
	struct dht_node *n = ((struct args *)arg)->node;
	struct hashtab *ht = (struct hashtab *)n->ht;
	struct timespec ts;
	char *key, *val;
	size_t val_size;

	while (!n->ready);

	sleep(5);
	(void)clock_gettime(CLOCK_REALTIME, &ts);
	for (i = 0 ; i < vnodes[me].n_keys ; i++) {
		if (!dht_put_tunable(n, vnodes[me].keys[i], vnodes[me].vals[i], &ts, N_REPLICAS))
			printf("FAILED [%s] dht_put_tunable.\n", n->id);
	}

	sleep(20);
	for (i = 0 ; i < TOTAL_KV ; i++) {
		if (hashtab_get(ht, all_keys[i], strlen(all_keys[i]) + 1, (void **)&val, &val_size) && !strcmp(val, all_vals[i])) {
			/*printf("OK\n");*/
		} else {
			fail++;
			printf("FAILED [%s] %s -> %s, got: %s -> %s\n", n->id, all_keys[i], all_vals[i], key, val);
		}
	}
	printf("SUCCESS [%s] %d/%d\n", n->id, TOTAL_KV - fail, TOTAL_KV);

	_exit(fail);
	/* NOTREACHED */
	return NULL;
}

int
node(int me)
{
	int i;
	struct dht_node *n;
	struct args *a;
	pthread_t tp, tt;

	n = malloc(sizeof(struct dht_node));
	if (!dht_init(n, vnodes[me].id, vnodes[me].port, N_REPLICAS, 0, NULL))
		err(1, "dht_init");

	for (i = 0 ; i < N_NODES ; i++)
		if (i != me)
			dht_add_peer(n, vnodes[i].id, vnodes[i].ip, vnodes[i].port);

	a = malloc(sizeof(struct args));
	a->node = n;
	a->nidx = me;
	if (pthread_create(&tt, NULL, test_thread, a))
		err(1, "pthread_create");

	dht_event_loop(n);
	errx(1, "should not reach here!");
	return 0;
}

int
main(int argc, char **argv)
{
	int i, status, fail = 0;
	uint32_t k, v;
	pid_t pid;

	/* generate nodes */
	vnodes = calloc(N_NODES, sizeof(struct v_node));
	for (i = 0 ; i < N_NODES ; i++) {
		asprintf(&(vnodes[i].id), "N%d", i);
		if (vnodes[i].id == NULL)
			err(1, "asprintf");
		vnodes[i].ip = strdup("127.0.0.1");
		if (vnodes[i].ip == NULL)
			err(1, "strdup");
		vnodes[i].port = PORT_BASE + i;	/* FIXME: check */
		k = arc4random();
		v = arc4random();
		/* TODO: add support to more than 1 KV per node */
		vnodes[i].n_keys = 1;
		asprintf(&(vnodes[i].keys[0]), "%ud", k);
		asprintf(&(vnodes[i].vals[0]), "%ud", v);
		if (vnodes[i].keys[0] == NULL || vnodes[i].vals[0] == NULL)
			err(1, "asprintf");
		all_keys[i] = strdup(vnodes[i].keys[0]);
		all_vals[i] = strdup(vnodes[i].vals[0]);
	}

	for (i = 0 ; i < N_NODES ; i++) {
		if (!fork()) {
			node(i);
			return 0;
		}
	}
	while (1) {
		pid = wait(&status);
		if (pid == -1) {
			if (errno == ECHILD)
				break;
			else
				err(1, "wait");
		} else {
			if (WIFEXITED(status) && WEXITSTATUS(status))
				fail++;
		}
	}
	sleep(5);
	return fail;
}
