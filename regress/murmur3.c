/*
 * Copyright (c) 2016 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <string.h>

#include <murmur3.h>

struct test {
	const char *input;
	uint64_t expected[2];
};

static struct test tests[] = {
	{.input = "", .expected = {0x0LLU, 0x0LLU}},
	{.input = "a", .expected = {0x85555565f6597889LLU, 0xe6b53a48510e895aLLU}},
	{.input = "$!", .expected = {0xbd5f7bc8a4fdd541LLU, 0x2638a8ef74dade00LLU}},
	{.input = "hello world!", .expected = {0x5aa80377fe21bbe3LLU, 0xedf56b1420cea7e7LLU}},
	{.input = "abracadabra", .expected = {0x3386097a32a76032LLU, 0x216f524eb811e738LLU}},
	{.input = "Abracadabra", .expected = {0x84c607dbc50d6e93LLU, 0x68532ed845d04979LLU}},
	{.input = "Lorem ipsum dolor sit amet.", .expected = {0x147b136c7bcdcd01LLU, 0x697f1d69b4d42a70LLU}},
	{.input = "Lorem ipsum dolor sit amet orci aliquam.", .expected = {0x3e19e6296c644dfbLLU, 0x91157457fc87a37bLLU}},
	{.input = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eu augue quis velit massa nunc.", .expected = {0xa1148cf5710c8478LLU, 0x1eccb6487c585ed0LLU}},
	{.input = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ultricies quam sit amet odio finibus ultricies. Integer vitae ante libero. Etiam gravida ac purus nec efficitur. Nam faucibus finibus velit, eu pulvinar nibh condimentum sed. Quisque tempor sed.", .expected = {0xe3787b529415e368LLU, 0xbab4b8be10f4213fLLU}}
};

int
main(int argc, char **argv)
{
	int i, fail = 0;
	uint64_t hash[2];

	for (i=0 ; i < 10 ; i++) {
		murmurhash3_x64_128(tests[i].input, strlen(tests[i].input), 0, hash);
		if ((hash[0] != tests[i].expected[0]) || (hash[1] != tests[i].expected[1])) {
			printf("FAILED \"%s\": 0x%llx:0x%llx should be 0x%llx:0x%llx\n", tests[i].input, hash[0], hash[1], tests[i].expected[0], tests[i].expected[1]);
			fail++;
		}
	}
	return fail;
}

