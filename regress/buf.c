/*
 * Copyright (c) 2016 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <buffer.h>
#include <sys/param.h>

int
main(int argc, char **argv)
{
	struct rbuffer *rb;
	char *str;
	int i;

	rb = rbuffer_new(10);
	if (rb == NULL)
		return 1;
	rbuffer_write(rb, "1234567890XY", 12);

	/* test: insertion */
	if (strncmp(rb->rb_data, "XY34567890", 10)) {
		printf("FAILED: ");
		for (i = 0 ; i < rb->rb_capacity ; i++)
			printf("%c ", rb->rb_data[i]);
		printf(" [%c]\n", *rb->rb_rp);
		return 1;
	}

	rbuffer_write(rb, "ABCDEFGHI", 9);
	if (strncmp(rb->rb_data, "IYABCDEFGH", 10)) {
		printf("FAILED: ");
		for (i = 0 ; i < rb->rb_capacity ; i++)
			printf("%c ", rb->rb_data[i]);
		printf(" [%c]\n", *rb->rb_rp);
		return 1;
	}

	rbuffer_write(rb, "J", 1);
	if (strncmp(rb->rb_data, "IJABCDEFGH", 10)) {
		printf("FAILED: ");
		for (i = 0 ; i < rb->rb_capacity ; i++)
			printf("%c ", rb->rb_data[i]);
		printf(" [%c]\n", *rb->rb_rp);
		return 1;
	}

	/* test: read */
	str = rbuffer_read(rb, 5);
	if (str == NULL)
		return 1;
	if (strncmp(str, "ABCDE", 5)) {
		printf("FAILED: ");
		for (i = 0 ; i < 5 ; i++)
			printf("%c ", str[i]);
		printf("\n");
		return 1;
	}
	free(str);

	str = rbuffer_read(rb, 5);
	if (str == NULL)
		return 1;
	if (strncmp(str, "FGHIJ", 5)) {
		printf("FAILED: ");
		for (i = 0 ; i < 5 ; i++)
			printf("%c ", str[i]);
		printf("\n");
		return 1;
	}
	free(str);

	/* test: read null */
	str = rbuffer_read(rb, 2);
	if (str != NULL)
		return 1;

	/* test: reset */
	rbuffer_write(rb, "mnopqrs", 7);
	str = rbuffer_read(rb, 5);
	if (str == NULL)
		return 1;
	if (strncmp(str, "mnopq", 5)) {
		printf("FAILED: ");
		for (i = 0 ; i < 5 ; i++)
			printf("%c ", str[i]);
		printf("\n");
		return 1;
	}
	free(str);

	/* test: readline */
	rbuffer_write(rb, "QWERTYUIOP\n", 11);
	str = rbuffer_readline(rb);
	if (str == NULL)
		return 1;
	if (strncmp(str, "WERTYUIOP", 10)) {
		printf("FAILED: ");
		for (i = 0 ; i < 10 ; i++)
			printf("%c ", str[i]);
		printf("\n");
		return 1;
	}
	free(str);

	rbuffer_write(rb, "\n", 1);
	str = rbuffer_readline(rb);
	if (str == NULL)
		return 1;
	if (strcmp(str, "")) {
		printf("FAILED\n");
		return 1;
	}

	rbuffer_free(rb);

	return 0;
}
