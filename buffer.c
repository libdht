/*
 * Copyright (c) 2016 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <buffer.h>
#include <sys/param.h>

struct rbuffer *
rbuffer_new(size_t capacity)
{
	struct rbuffer *rbuf;

	rbuf = malloc(sizeof(struct rbuffer));
	if (rbuf == NULL)
		goto ret;
	rbuf->rb_capacity = capacity > 0 ? capacity : BUFSIZ;
	rbuf->rb_len = 0;
	rbuf->rb_data = reallocarray(NULL, rbuf->rb_capacity, sizeof(char));
	if (rbuf->rb_data == NULL) {
		free(rbuf);
		goto ret;
	}
	rbuf->rb_wp = rbuf->rb_data;
	rbuf->rb_rp = rbuf->rb_data;
ret:
	return rbuf;
}

void
rbuffer_free(struct rbuffer *rbuf)
{
	if (rbuf == NULL)
		return;
	free(rbuf->rb_data);
	free(rbuf);
}

int
rbuffer_write(struct rbuffer *rbuf, const char *src, size_t len)
{
	int crossed = 0;
	size_t block, n, copied = 0, free, rem = len;

	if (rbuf == NULL)
		return -1;

	free = rbuf->rb_capacity - rbuf->rb_len;
	if (len > free)
		crossed = 1;

	/* TODO: optimize, copy only what needs to be copied. */
	while (rem) {
		block = rbuf->rb_capacity - (rbuf->rb_wp - rbuf->rb_data);
		n = MIN(rem, block);
		memcpy(rbuf->rb_wp, src + copied, n);
		rbuf->rb_len += n;
		copied += n;

		rbuf->rb_wp += n;
		if (rbuf->rb_wp >= rbuf->rb_data + rbuf->rb_capacity)
			rbuf->rb_wp = rbuf->rb_data;

		rem -= n;
	}

	/* check if wp crossed rp */
	if (crossed)
		rbuf->rb_rp = rbuf->rb_wp;

	rbuf->rb_len = MIN(rbuf->rb_len, rbuf->rb_capacity);

	return 0;
}

char *
rbuffer_read(struct rbuffer *rbuf, size_t len)
{
	char	*str;
	size_t	total, n;

	total = MIN(len, rbuf->rb_len);
	if (!total)
		return NULL;
	str = reallocarray(NULL, total, sizeof(char));

	n = MIN(len, (rbuf->rb_data + rbuf->rb_capacity) - rbuf->rb_rp);
	memcpy(str, rbuf->rb_rp, n);
	rbuf->rb_rp += n;
	
	if (n < total) {
		memcpy(str + n, rbuf->rb_data, total - n);
		rbuf->rb_rp = rbuf->rb_data + (total - n);
	}

	rbuf->rb_len -= total;

	return str;
}

static int
rbuffer_find(struct rbuffer *rbuf, char ch, size_t *len)
{
	char	*ptr = rbuf->rb_rp;
	size_t	 n, m;

	n = MIN(rbuf->rb_len, (rbuf->rb_data + rbuf->rb_capacity) - rbuf->rb_rp);
	m = rbuf->rb_len - n;
	for (ptr = rbuf->rb_rp ; n-- ; ptr++) {
		if (*ptr == ch) {
			*len = rbuf->rb_len - n - m;
			return 1;
		}
	}
	for (ptr = rbuf->rb_data ; m-- ; ptr++) {
		if (*ptr == ch) {
			*len = rbuf->rb_len - m;
			return 1;
		}
	}
	return 0;
}

char *
rbuffer_readline(struct rbuffer *rbuf)
{
	char	*str;
	size_t	 n;

	if (!rbuf->rb_len)
		return NULL;
	if (rbuffer_find(rbuf, '\n', &n)) {
		str = rbuffer_read(rbuf, n);
		str[n - 1] = '\0';
		return str;
	}
	return NULL;
}
