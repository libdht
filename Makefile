LIB=		dht
SRCS=		dht.c murmur3.c buffer.c
HDRS=		dht.h
SHLIB_MAJOR=	0
SHLIB_MINOR=	1
#MAN=		libdht.3
#MLINKS=	libdht.3 dht_init.3 \
#		libdht.3 dht_add_peer.3 \
#		libdht.3 dht_put_tunable.3 \
#		libdht.3 dht_get_tunable.3 \
#		libdht.3 dht_put.3 \
#		libdht.3 dht_get.3 \
#		libdht.3 dht_event_loop.3 \

#CFLAGS+=	-Wall -Werror -I.
CFLAGS+=	-Wall -I.
COPTS+=		-g #-DDEBUG


includes:
	@cd ${.CURDIR}; for i in $(HDRS); do \
	    j="cmp -s $$i ${DESTDIR}/usr/include/$$i || \
		${INSTALL} ${INSTALL_COPY} -o ${BINOWN} -g ${BINGRP} \
		-m 444 $$i ${DESTDIR}/usr/include"; \
	    echo $$j; \
	    eval "$$j"; \
	done

.include <bsd.lib.mk>
