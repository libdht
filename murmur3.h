/*
 * based on code written by Austin Appleby and placed in the public domain
 */

#ifndef MURMUR3_H
#define MURMUR3_H

#include <stdint.h>

void murmurhash3_x64_128(const void *, const int, const uint32_t, void *);

#endif
